//
//  CategoryCollectionViewCell.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgProducts: UIImageView!
    @IBOutlet var lblCategory: UILabel!
}
