//
//  ProductCollectionViewCell.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/26/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var txtItemName: UILabel!
    @IBOutlet var txtBrandname: UILabel!
    @IBOutlet var txtSRP: UILabel!
}
