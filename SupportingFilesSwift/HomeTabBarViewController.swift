//
//  HomeTabBarViewController.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var state: userState = .user
    var listViewController = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        populateTabBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateTabBar(){
        
        // check if userdefaults with key "role"
        if let role = UserDefaults.standard.string(forKey: "role") {            
            switch role {
            case "1":
                state = userState.admin
            case "3":
                state = userState.user
            default:
                state = userState.incorrect_input
            }
        }
        
        switch state{
        case .user:
            listViewController = ["Categories", "Search", "Scan", "Cart", "Profile"]
        case .admin:
            listViewController = ["Add", "AddBarCode", "Profile"]
        default:
            //default as user
            listViewController = ["Categories", "Search", "Scan", "Cart", "Profile"]
        }
        
        var tempvc = UIViewController()
        
        var viewControllerss = [UIViewController]()
        for str in listViewController{
            tempvc = (self.storyboard?.instantiateViewController(withIdentifier: str))!
            viewControllerss.append(tempvc)
        }
        self.viewControllers = viewControllerss
        
        
    }

}
