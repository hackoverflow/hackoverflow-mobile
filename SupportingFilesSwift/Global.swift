//
//  Global.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import Foundation
import UIKit
var actInd : UIActivityIndicatorView = UIActivityIndicatorView()
func showLoadingMode (loading : UIViewController){
    
    actInd = UIActivityIndicatorView(activityIndicatorStyle: .white)
    actInd.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    actInd.center = loading.view.center
    actInd.hidesWhenStopped = true
    actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
    loading.view.addSubview(actInd)
    actInd.startAnimating()
    
}
