//
//  Extensions.swift
//  GrocerbUD
//
//  Created by Ma. Ciela Salazar on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

extension UIViewController{
    
    func simpleAlert(domain: String, message: String) {
        let alert = UIAlertController(title: domain, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
