//
//  LoginViewController.swift
//  GrocerbUD
//
//  Created by Ma. Ciela Salazar on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum userState{
    case user, admin, incorrect_input
}
class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var loginMode: userState = .incorrect_input
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:
            
            #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func loginModule(){
        guard let username = usernameTextField.text, let password = passwordTextField.text
            else {
                return
        }
        let url = "http://67.205.152.118/api/v1/sessions/login"
        let params = [
            "email" : username,
            "password" : password
        ]
        Alamofire.request(url, method: .post, parameters: params).response { response in
            debugPrint(response)
        }
            .responseJSON { response in
                if (response.result.isSuccess){
                    if let j = response.result.value {
                        let json = JSON(j)
                        print(json)
                     let role = json["role_id"].stringValue
                        if role == "1"{
                            OperationQueue.main.addOperation{
                                UserDefaults.standard.set(role, forKey: "role")
                            self.loginMode = .admin
                            self.stateAction(state: .admin)
                            }
                            
                            
                        } else if role == "3"{
                            OperationQueue.main.addOperation{
                                UserDefaults.standard.set(role, forKey: "role")
                            self.loginMode = .user
                            self.stateAction(state: .user)
                            }
                        }else{
                            self.loginMode = .incorrect_input
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "login_success"), object: nil)
                            //            UserDefaults.standard.set(false, forKey: "login_success")
                            self.simpleAlert(domain: "ERROR", message: "Wrong Username or Password")
                        }
                    }
                    
                } else if (response.result.isFailure) {
                    //Manager your error
                    switch (response.error!._code){
                    case NSURLErrorTimedOut:
                        print("Connection Time out")
                        break
                    case NSURLErrorNotConnectedToInternet:
                        print ("No Internet Connection")
                        break
                    default:
                        print("Error Credentials")
                        break
                    }
                }
                debugPrint(response)
                
        }
 
       
    }

    @IBAction func userLogin(_ sender: UIButton) {
        loginModule()
        }

    func stateAction(state: userState){
        switch state{
        case .user:
            fallthrough
        case .admin:
     UserDefaults.standard.set(true, forKey: "login_success")
     performSegue(withIdentifier: "tabbarsegue", sender: state)
        
        case .incorrect_input:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "login_success"), object: nil)
//            UserDefaults.standard.set(false, forKey: "login_success")
            simpleAlert(domain: "ERROR", message: "Wrong Username or Password")
        }
        
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tabbarsegue"{
            if let vc = segue.destination as? HomeTabBarViewController{
                vc.state = loginMode
                
            }
        }
    }
    

}
