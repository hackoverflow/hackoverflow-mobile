//
//  ProfileViewController.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func logOut(_ sender: UIButton) {
        let alertMsg = UIAlertController(title: "Warning", message: "Are you sure you want to log out your account ?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertMsg.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action: UIAlertAction!) in
            UserDefaults.standard.removeObject(forKey: "role")
            UserDefaults.standard.synchronize()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController: AnyObject! = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = tabBarController as? UIViewController
        }))
        
        alertMsg.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            alertMsg.dismiss(animated: true, completion: nil)
        }))
        
        present(alertMsg, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
