//
//  CategoryViewController.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON
class CategoryViewController: UIViewController {
    
    var category = [[String: AnyObject?]]()
    var strCategoryID = [Int]()
    var strImgURL = [String]()
    var strCategory = [String]()
    var txtCategoryID = ""
    var txtImgURL = ""
    var txtCategory = ""

    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        loadJSON()
    }
    func loadJSON(){
        self.strImgURL.removeAll()
        self.strCategory.removeAll()
        let url = "http://67.205.152.118/api/v1/categories/index"
        Alamofire.request(url, method: .get).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let json = JSON(responseData.result.value!)
                print(json)
                
                if let arrayCategory = json.arrayObject {
                    self.category = arrayCategory as! [[String : AnyObject]]
                    
                    // to avoid crashing
                    
                    for forCategory in self.category {
                        
                        if let image = forCategory["image"]{
                            self.strCategoryID.append(forCategory["id"] as! Int)
                            self.strCategory.append(forCategory["name"] as! String)
                            if let imgUrl = image?["url"]{
                                let convertedURL = String(describing: imgUrl)
                                self.strImgURL.append(convertedURL)
print("convertedimg", convertedURL)

                            }
                            
                        }
                       
                        
                    }
                    self.collectionView.reloadData()
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
extension CategoryViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func initialize(){
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
  /*  func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.strCategoryID.count
    }*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.strCategoryID.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category", for: indexPath) as! CategoryCollectionViewCell

        
        cell.lblCategory.text = self.strCategory[indexPath.row]
        let baseURL = "http://67.205.152.118"
       
        var imageURL = self.strImgURL[indexPath.row].replacingOccurrences(of: "Optional(", with: "")
        imageURL = imageURL.replacingOccurrences(of: ")", with: "")
        
        
        cell.imgProducts.downloadedFrom(link:  baseURL + imageURL )
        print("image:", baseURL + imageURL)
        cell.contentView.layer.borderWidth = 1
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView?.cellForItem(at: indexPath as IndexPath)  as! CategoryCollectionViewCell
        
        self.performSegue(withIdentifier: "segue", sender: self.strCategoryID[indexPath.row])

        
        // access to any property inside the cell you have.
        // cell.usernameLabel.text
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue"
        {
            let vc = segue.destination as! ProductListViewController
            vc.strCategoryID = sender as! Int
        }
        
    }
    
}
    
   // *****//search//////////////////////////
    ///func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // to limit network activity, reload half a second after last key press.
//        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: "reload", object: nil)
//        self.performSelector("reload", withObject: nil, afterDelay: 0.5)
        
        // get searchText
        
        // call Search API, parameter searchString
        //searchApi()
   // }


//image sd image or nuke


