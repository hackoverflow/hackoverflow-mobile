//
//  ProductListViewController.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/26/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductListViewController: UIViewController {
    var strCategoryID = Int()
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("productID", strCategoryID)
        initializeProduct()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func initializeProduct(){
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return self.strCategoryID.count
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productcategory", for: indexPath) as! ProductCollectionViewCell
        
        
        cell.txtItemName.text = "Sample Item Name" //self.strCategory[indexPath.row]
        cell.txtBrandname.text = "Sample Brand Name"
        cell.txtSRP.text = "Sample Price"
        cell.imgProduct.downloadedFrom(link: "ttp://67.205.152.118/uploads/product/image/27/Screen_Shot_2017-10-21_at_10.24.26_PM.png")
        cell.contentView.layer.borderWidth = 1
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        return cell
    }
   /*
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView?.cellForItem(at: indexPath as IndexPath)  as! CategoryCollectionViewCell
        
        self.performSegue(withIdentifier: "segue", sender: self.strCategoryID[indexPath.row])
        
        
        // access to any property inside the cell you have.
        // cell.usernameLabel.text
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue"
        {
            let vc = segue.destination as! ProductListViewController
            vc.strCategoryID = sender as! Int
        }
        
    }
 */
    
}

