//
//  SearchTableViewCell.swift
//  GrocerbUD
//
//  Created by Caroline Ignacio on 11/25/17.
//  Copyright © 2017 Jump Digital Asia Technical Team. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var txtItemName: UILabel!
    @IBOutlet var txtBrand: UILabel!
    @IBOutlet var txtPrice: UILabel!
    @IBOutlet var txtCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
